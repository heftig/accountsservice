# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Cédric Valmary <cvalmary@yahoo.fr>, 2015
msgid ""
msgstr ""
"Project-Id-Version: accounts service\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/accountsservice/"
"accountsservice/issues\n"
"POT-Creation-Date: 2020-05-26 15:24+0000\n"
"PO-Revision-Date: 2021-05-28 22:06+0200\n"
"Last-Translator: Quentin PAGÈS\n"
"Language-Team: Occitan (post 1500) (http://www.transifex.com/freedesktop/"
"accountsservice/language/oc/)\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 2.4.3\n"

#: data/org.freedesktop.accounts.policy.in:11
msgid "Change your own user data"
msgstr "Modificar sas pròprias donadas"

#: data/org.freedesktop.accounts.policy.in:12
msgid "Authentication is required to change your own user data"
msgstr ""
"Vos cal vos autentificar per modificar vòstras pròprias donadas d'utilizaire"

#: data/org.freedesktop.accounts.policy.in:21
msgid "Change your own user password"
msgstr "Modificar son pròpri senhal"

#: data/org.freedesktop.accounts.policy.in:22
msgid "Authentication is required to change your own user password"
msgstr "Devètz vos autentificar per modificar vòstre pròpri senhal"

#: data/org.freedesktop.accounts.policy.in:31
msgid "Manage user accounts"
msgstr "Gerir los comptes dels utilizaires"

#: data/org.freedesktop.accounts.policy.in:32
msgid "Authentication is required to change user data"
msgstr "Es necessari de s'autentificar per modificar de donadas d'utilizaire"

#: data/org.freedesktop.accounts.policy.in:41
msgid "Change the login screen configuration"
msgstr "Modificar la configuracion de l'ecran de connexion"

#: data/org.freedesktop.accounts.policy.in:42
msgid "Authentication is required to change the login screen configuration"
msgstr ""
"Es necessari de s'autentificar per modificar la configuracion de l'ecran de "
"connexion"

#: src/main.c:201
msgid "Output version information and exit"
msgstr "Aficha la version e quita"

#: src/main.c:202
msgid "Replace existing instance"
msgstr "Remplaça una instància existenta"

#: src/main.c:203
msgid "Enable debugging code"
msgstr "Activa lo còde de desbugatge"

#: src/main.c:222
msgid ""
"Provides D-Bus interfaces for querying and manipulating\n"
"user account information."
msgstr ""
"Provesís d'interfàcias D-Bus per interrogar e manipular\n"
"d'informacions suls comptes dels utilizaires."
